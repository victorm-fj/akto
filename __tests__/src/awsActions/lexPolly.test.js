import {
  pollyVoices,
  getLocale /*getAudioUrl*/,
} from '../../../src/awsActions';

describe('AWS Polly', () => {
  test('map user locale string to one of polly voices', () => {
    const userLocale = 'en-MX';
    const mappedLocale = 'en-US';
    const voiceId = getLocale(userLocale);
    expect(voiceId).toBe(pollyVoices[mappedLocale][0]);
  });
});
