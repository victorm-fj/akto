import React from 'react';
import { StyleSheet, View, Text, TextInput, Button } from 'react-native';
import { Auth } from 'aws-amplify';
import { validatePassword } from '../utils/helpers';

const requiredPasswordText = 'Please fill out your password!';
const matchPasswordText = 'Passwords do not macth!';
const errorPasswordText =
	'Invalid password!\nPassword must contain numbers, at least one special character, uppercase and lowercase letters, and be at least 8 characters long.';
const requiredText = 'Please fill out your confirmation code!';
const errorText =
	'This code isn\'t valid. Please copy and paste the code the SMS you received.';

class ForgotPassword extends React.Component {
	state = {
	  password: '',
	  passwordConfirm: '',
	  passwordError: '',
	  passwordConfirmError: '',
	  submitDisabled: true,
	  passwordConfirmDisabled: true,
	  loading: false,
	  authError: '',
	  code: '',
	  codeError: '',
	  codeDisabled: true,
	};
	validatePassword = () => {
	  const { password } = this.state;
	  if (!password) {
	    this.setState({ passwordError: requiredPasswordText });
	  } else {
	    if (validatePassword(password)) {
	      this.setState({ passwordError: '' });
	    } else {
	      this.setState({ passwordError: errorPasswordText });
	    }
	  }
	};
	comparePasswords = () => {
	  const { password, passwordConfirm, passwordError } = this.state;
	  if (passwordError) this.setState({ submitDisabled: true });
	  else if (password !== passwordConfirm) {
	    this.setState({
	      passwordConfirmError: matchPasswordText,
	      submitDisabled: true,
	    });
	  } else {
	    this.setState({ passwordConfirmError: '', submitDisabled: false });
	  }
	};
	validateCode = () => {
	  const { code } = this.state;
	  if (!code) {
	    this.setState({ codeError: requiredText, codeDisabled: true });
	  } else {
	    if (code.length === 6 && !isNaN(code)) {
	      this.setState({ codeError: '', codeDisabled: false });
	    } else {
	      this.setState({ codeError: errorText, codeDisabled: true });
	    }
	  }
	};
	onChangePassword = (password) => {
	  this.setState({ password: password.trim() });
	};
	onChangePasswordConfirm = (passwordConfirm) => {
	  this.setState({ passwordConfirm: passwordConfirm.trim() });
	};
	onChangeTextHandler = (code) => {
	  console.log(code);
	  this.setState({ code: code.trim() });
	};
	onPressHandler = async () => {
	  const { steps, triggerNextStep } = this.props;
	  const { password, code } = this.state;
	  const username = steps.accessId.value.trim();
	  console.log('forgotPassword credentials', username, code, password);
	  try {
	    await Auth.forgotPasswordSubmit(username, code, password);
	    triggerNextStep({ value: undefined, trigger: 'signIn' });
	  } catch (error) {
	    console.log('forgotPasswordSubmit error', error);
	    this.setState({ authError: error.message });
	  }
	};
	render() {
	  console.log('Password props', this.props);
	  const {
	    password,
	    passwordConfirm,
	    submitDisabled,
	    passwordConfirmDisabled,
	    passwordError,
	    passwordConfirmError,
	    authError,
	    code,
	    codeError,
	  } = this.state;
	  return (
	    <View style={{ justifyContent: 'center' }}>
	      <Text style={{ padding: 5 }}>
					Enter the code you received and set a new password.
	      </Text>
	      <TextInput
	        style={{ height: 40 }}
	        onChangeText={this.onChangePassword}
	        placeholder="Password..."
	        value={password}
	        onBlur={this.validatePassword}
	        secureTextEntry
	      />
	      <TextInput
	        style={{ height: 40 }}
	        onChangeText={this.onChangePasswordConfirm}
	        placeholder="Confirm password..."
	        value={passwordConfirm}
	        editable={passwordConfirmDisabled}
	        onBlur={this.comparePasswords}
	        secureTextEntry
	      />
	      <TextInput
	        value={code}
	        placeholder="Enter confirmation code..."
	        onChangeText={this.onChangeTextHandler}
	        onBlur={this.validateCode}
	      />
	      {!!codeError && <Text style={styles.errorMessage}>{codeError}</Text>}
	      <Text style={styles.errorMessage}>{passwordError}</Text>
	      <Text style={styles.errorMessage}>{passwordConfirmError}</Text>
	      <Text style={styles.errorMessage}>{authError}</Text>
	      <Button
	        title="Set new password"
	        onPress={this.onPressHandler}
	        disabled={submitDisabled}
	      />
	    </View>
	  );
	}
}

const styles = StyleSheet.create({
  errorMessage: {
    color: 'rgb(199, 0, 57)',
    padding: 5,
  },
});
export default ForgotPassword;
