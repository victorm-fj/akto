import React from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';
import { Auth } from 'aws-amplify';
import { validatePassword } from '../utils/helpers';

const requiredPasswordText = 'Please fill out your password!';
const matchPasswordText = 'Passwords do not macth!';
const errorPasswordText = 'Invalid password!';

class Password extends React.Component {
	state = {
	  password: '',
	  passwordConfirm: '',
	  passwordError: '',
	  passwordConfirmError: '',
	  submitDisabled: true,
	  passwordConfirmDisabled: true,
	  loading: false,
	  authError: '',
	};
	validatePassword = () => {
	  const { password } = this.state;
	  if (!password) {
	    this.setState({ passwordError: requiredPasswordText });
	  } else {
	    if (validatePassword(password)) {
	      this.setState({ passwordError: '' });
	    } else {
	      this.setState({ passwordError: errorPasswordText });
	    }
	  }
	};
	comparePasswords = () => {
	  const { password, passwordConfirm, passwordError } = this.state;
	  if (passwordError) this.setState({ submitDisabled: true });
	  else if (password !== passwordConfirm) {
	    this.setState({
	      passwordConfirmError: matchPasswordText,
	      submitDisabled: true,
	    });
	  } else {
	    this.setState({ passwordConfirmError: '', submitDisabled: false });
	  }
	};
	onPressHandler = async () => {
	  const { steps, triggerNextStep } = this.props;
	  const { password } = this.state;
	  const { username } = JSON.parse(steps.accessId.value);
	  const phoneNumber = steps.phoneNumber.value.trim();
	  console.log('signUp credentials', username, password, phoneNumber);
	  try {
	    await Auth.signUp({
	      username,
	      password,
	      attributes: { phone_number: phoneNumber },
	    });
	    triggerNextStep({ value: password, trigger: '15' });
	  } catch (error) {
	    console.log('signUp error', error);
	    this.setState({ authError: error.message });
	  }
	};
	onChangePassword = (password) => {
	  this.setState({ password: password.trim() });
	};
	onChangePasswordConfirm = (passwordConfirm) => {
	  this.setState({ passwordConfirm: passwordConfirm.trim() });
	};
	onChangePhoneNumber = () => {
	  const { triggerNextStep } = this.props;
	  triggerNextStep({ value: undefined, trigger: 'phoneNumber' });
	};
	onPressSignIn = () => {
	  const { triggerNextStep } = this.props;
	  triggerNextStep({ value: undefined, trigger: '1' });
	};
	render() {
	  console.log('Password props -', this.props);
	  const {
	    password,
	    passwordConfirm,
	    submitDisabled,
	    passwordConfirmDisabled,
	    passwordError,
	    passwordConfirmError,
	    authError,
	  } = this.state;
	  return (
	    <View style={{ justifyContent: 'center' }}>
	      <TextInput
	        style={{ height: 40 }}
	        onChangeText={this.onChangePassword}
	        placeholder="Password..."
	        value={password}
	        onBlur={this.validatePassword}
	        secureTextEntry
	      />
	      <TextInput
	        style={{ height: 40 }}
	        onChangeText={this.onChangePasswordConfirm}
	        placeholder="Confirm password..."
	        value={passwordConfirm}
	        editable={passwordConfirmDisabled}
	        onBlur={this.comparePasswords}
	        secureTextEntry
	      />
	      <Text style={styles.errorMessage}>{passwordError}</Text>
	      <Text style={styles.errorMessage}>{passwordConfirmError}</Text>
	      <Text style={styles.errorMessage}>{authError.substring(28)}</Text>
	      <Button
	        title="Join Akto"
	        onPress={this.onPressHandler}
	        disabled={submitDisabled}
	      />
	      {this.state.authError.indexOf('phone number') >= 0 && (
	        <View>
	          <Button
	            title="Change phone number"
	            onPress={this.onChangePhoneNumber}
	          />
	          <Button title="Sign in" onPress={this.onPressSignIn} />
	        </View>
	      )}
	    </View>
	  );
	}
}

const styles = StyleSheet.create({
  errorMessage: {
    color: 'rgb(199, 0, 57)',
    padding: 5,
  },
});
export default Password;
