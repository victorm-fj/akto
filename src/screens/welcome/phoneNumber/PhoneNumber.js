import React from 'react';
import { View, Button, Alert } from 'react-native';
import CarrierInfo from 'react-native-carrier-info';
import PhoneInput from 'react-native-phone-input';
import CountryPicker from 'react-native-country-picker-modal';

class PhoneNumber extends React.Component {
	state = { initialCountry: undefined, phoneNumber: '', cca2: undefined };
	async componentWillMount() {
	  const { steps } = this.props;
	  if (typeof steps.phoneNumber.value !== 'undefined') {
	    this.setState({ phoneNumber: steps.phoneNumber.value });
	  }
	  try {
	    const initialCountry = await CarrierInfo.isoCountryCode();
	    console.log('initialCountry', initialCountry);
	    this.setState({ initialCountry, cca2: initialCountry.toUpperCase() });
	  } catch (error) {
	    console.log('error', error);
	    this.setState({ initialCountry: 'us', cca2: 'US' });
	  }
	}
	componentDidMount() {
	  if (this.phone) {
	    this.setState({
	      pickerData: this.phone.getPickerData(),
	    });
	  }
	}
	onPressFlag = () => {
	  this.countryPicker.openModal();
	};
	selectCountry = (country) => {
	  this.phone.selectCountry(country.cca2.toLowerCase());
	  this.setState({ cca2: country.cca2 });
	};
	onPressHandler = () => {
	  const { triggerNextStep } = this.props;
	  const validNumber = this.phone.isValidNumber();
	  const phoneNumber = this.phone.getValue();
	  console.log('validNumber', validNumber);
	  if (validNumber) {
	    triggerNextStep({ value: phoneNumber, trigger: '13' });
	  } else {
	    const validationMessage = 'Please enter a valid phone number.';
	    Alert.alert('Invalid phone number', validationMessage, [
	      { text: 'OK', onPress: () => {} },
	    ]);
	  }
	};
	render() {
	  if (typeof this.state.initialCountry !== 'undefined') {
	    return (
	      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
	        <PhoneInput
	          ref={(phone) => (this.phone = phone)}
	          textStyle={{ color: '#000' }}
	          initialCountry={this.state.initialCountry}
	          value={this.state.phoneNumber}
	          style={{ marginBottom: 10, flex: 1 }}
	          textProps={{ placeholder: 'Enter phone number' }}
	          onPressFlag={this.onPressFlag}
	          offset={15}
	        />
	        <Button title="Ok" onPress={this.onPressHandler} />
	        <CountryPicker
	          ref={(countryPicker) => (this.countryPicker = countryPicker)}
	          onChange={this.selectCountry}
	          cca2={this.state.cca2}
	          onClose={() => {}}
	        >
	          <View />
	        </CountryPicker>
	      </View>
	    );
	  }
	  return null;
	}
}

export default PhoneNumber;
