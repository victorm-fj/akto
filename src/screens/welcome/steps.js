import React from 'react';
import AccessIdContainer from './accessId/AccessIdContainer';
import PhoneNumber from './phoneNumber/PhoneNumber';
import Password from './password/Password';
import ConfirmAccountContainer from './confirmAccount/ConfirmAccountContainer';
import SignIn from './signIn/SignIn';
import ForgotPassword from './password/ForgotPassword';
import { strings } from '../../locales/i18n';

export default [
  {
    id: '1',
    message: strings('welcome.hi'), //'Hi! Welcome to Akto.\nWhat\'s your Access ID?',
    trigger: 'accessId',
  },
  {
    id: 'accessId',
    user: true,
    trigger: '2',
  },
  {
    id: '2',
    component: <AccessIdContainer />,
    asMessage: true,
    replace: true,
    waitAction: true,
  },
  {
    id: '3',
    message: strings('welcome.invalidAccessId'),
    trigger: 'accessId',
  },
  {
    id: '4',
    message: ({ previousValue, steps }) => {
      const value = JSON.parse(previousValue);
      return strings('welcome.validAccessId', { value: value.username });
    },
    trigger: 'yesNo',
  },
  {
    id: 'yesNo',
    options: [
      { value: 'no', label: strings('welcome.optionPhoneNo'), trigger: '5' },
      { value: 'yes', label: strings('welcome.optionPhoneYes'), trigger: '17' },
    ],
  },
  {
    id: '5',
    message: ({ previousValue, steps }) => {
      const value = JSON.parse(steps.accessId.value);
      return strings('welcome.phoneNoResponse', { value: value.username });
    },
    trigger: 'introduction',
  },
  {
    id: 'introduction',
    options: [
      { value: 'continue', label: strings('welcome.continue'), trigger: '7' },
    ],
  },
  {
    id: '7',
    message: strings('welcome.introduction'),
    trigger: 'agency',
  },
  {
    id: 'agency',
    options: [
      {
        value: 'intelligence agency',
        label: strings('welcome.optionIntelligence'),
        trigger: '9',
      },
      {
        value: 'want to join',
        label: strings('welcome.optionJoin'),
        trigger: '11',
      },
    ],
  },
  {
    id: '9',
    message: strings('welcome.agency'),
    trigger: 'join',
  },
  {
    id: 'join',
    options: [
      {
        value: 'join',
        label: strings('welcome.optionWantToJoin'),
        trigger: '11',
      },
    ],
  },
  {
    id: '11',
    message: strings('welcome.join'),
    trigger: 'phoneNumber',
  },
  {
    id: 'phoneNumber',
    component: <PhoneNumber />,
    replace: true,
    waitAction: true,
  },
  {
    id: '13',
    message: ({ previousValue, steps }) =>
      strings('welcome.confirmPhone', { value: previousValue }),
    trigger: 'editPhone',
  },
  // {
  //   id: '14',
  //   message:
  // 		'It seems that you entered an invalid phone number, please enter a valid one.',
  //   trigger: 'phoneNumber',
  // },
  {
    id: 'editPhone',
    options: [
      {
        value: 'edit',
        label: strings('welcome.editPhoneYes'),
        trigger: 'phoneNumber',
      },
      { value: 'yes', label: strings('welcome.editPhoneNo'), trigger: '18' },
    ],
  },
  {
    id: '15',
    message: strings('welcome.sentCode'),
    trigger: 'confirmAccount',
  },
  {
    id: '17',
    message: strings('welcome.phoneYesResponse'),
    trigger: 'phoneNumber',
  },
  {
    id: 'updatePhone',
    update: 'phoneNumber',
    trigger: ({ value, steps }) => {
      console.log('value', value, 'steps', steps);
      return 'phoneNumber';
    },
  },
  {
    id: '18',
    message: strings('welcome.setPassword'),
    trigger: 'setPassword',
  },
  {
    id: 'setPassword',
    component: <Password />,
    waitAction: true,
    replace: true,
  },
  {
    id: 'confirmAccount',
    component: <ConfirmAccountContainer />,
    waitAction: true,
    replace: true,
  },
  {
    id: '19',
    message: ({ previousValue, steps }) =>
      strings('welcome.signIn', { value: previousValue }),
    trigger: 'signIn',
  },
  {
    id: 'signIn',
    component: <SignIn />,
    replace: true,
    waitAction: true,
  },
  {
    id: 'forgotPassword',
    component: <ForgotPassword />,
    replace: true,
    waitAction: true,
  },
  {
    id: 'end',
    message: strings('welcome.end'),
    end: true,
  },
];
