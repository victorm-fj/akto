import { graphql } from 'react-apollo';
import NewUserMutation from '../../../mutations/NewUserMutation';
import ConfirmAccount from './ConfirmAccount';

export default graphql(NewUserMutation, {
  options: {
    fetchPolicy: 'no-cache',
  },
  props: ({ ownProps, mutate }) => ({
    onNewUser: (username, invitation) =>
      mutate({
        variables: {
          username,
          invitation,
        },
        optimisticResponse: {
          __typename: 'Mutation',
          newUser: {
            __typename: 'UserType',
            userId: '',
            username,
            invitationId: '',
            recruiter: '',
            recruits: [],
          },
        },
      }),
  }),
})(ConfirmAccount);
