import React from 'react';
import {
  View,
  Button,
  TextInput,
  Text,
  Platform,
  StyleSheet,
} from 'react-native';
import { Auth } from 'aws-amplify';
import { withNavigation } from 'react-navigation';
import SmsListener from 'react-native-android-sms-listener';

const requiredText = 'Please fill out your confirmation code!';
const errorText =
	'This code isn\'t valid. Please copy and paste the code the SMS you received.';

class ConfirmAccount extends React.Component {
	state = {
	  code: '',
	  error: '',
	  disabled: true,
	  authError: '',
	  resendMessage: '',
	};
	componentDidMount() {
	  if (Platform.OS === 'android') {
	    this.smsListener = SmsListener.addListener((message) => {
	      const verificationCodeRegex = /[\d]{6}/;
	      if (verificationCodeRegex.test(message.body)) {
	        const verificationCode = message.body.match(verificationCodeRegex)[0];
	        console.log('verification code: ' + verificationCode);
	        this.setState({ code: verificationCode, error: '', disabled: false });
	        setTimeout(() => this.onPressHandler(), 1000);
	      }
	    });
	  }
	}
	componentWillUnmount() {
	  if (Platform.OS === 'android') {
	    this.smsListener.remove();
	  }
	}
	validateCode = () => {
	  const { code } = this.state;
	  if (!code) {
	    this.setState({ error: requiredText, disabled: true });
	  } else {
	    if (code.length === 6 && !isNaN(code)) {
	      this.setState({ error: '', disabled: false });
	    } else {
	      this.setState({ error: errorText, disabled: true });
	    }
	  }
	};
	onPressHandler = async () => {
	  console.log('submit code');
	  const { code } = this.state;
	  const { steps, triggerNextStep, onNewUser, navigation } = this.props;
	  const { codeId, username } = JSON.parse(steps.accessId.value);
	  const password = steps.setPassword.value.trim();
	  try {
	    await Auth.confirmSignUp(username, code);
	    await Auth.signIn(username, password);
	    const user = await onNewUser(username, codeId);
	    console.log('new User', user);
	    triggerNextStep({ value: undefined, trigger: 'end' });
	    setTimeout(() => navigation.navigate('Main'), 1500);
	  } catch (error) {
	    console.log('confirmSignUp error', error);
	    this.setState({ authError: error, resendMessage: '' });
	  }
	};
	onChangeTextHandler = (code) => {
	  console.log(code);
	  this.setState({ code: code.trim() });
	};
	onPressResend = async () => {
	  try {
	    const { username } = JSON.parse(this.props.steps.accessId.value);
	    await Auth.resendSignUp(username);
	    console.log('resendCode action fired');
	    this.setState({
	      resendMessage:
					'A new confirmation code has been send to your phone number',
	    });
	  } catch (error) {
	    console.log('resendCode error -', error);
	    this.setState({ authError: error.message, resendMessage: '' });
	  }
	};
	onPressUpdatePhone = () => {
	  this.props.triggerNextStep({ value: undefined, trigger: 'phoneNumber' });
	};
	render() {
	  console.log('ConfirmAccount props -', this.props);
	  const { code, error, disabled, authError, resendMessage } = this.state;
	  return (
	    <View style={{ justifyContent: 'center' }}>
	      <Text style={{ padding: 5 }}>
					Confirmation code sent to {this.props.steps.phoneNumber.value}
	      </Text>
	      <TextInput
	        value={code}
	        placeholder="Enter confirmation code..."
	        onChangeText={this.onChangeTextHandler}
	        onBlur={this.validateCode}
	      />
	      {!!error && <Text style={styles.errorMessage}>{error}</Text>}
	      {!!authError && <Text style={styles.errorMessage}>{authError}</Text>}
	      <Button
	        title="Confirm account"
	        onPress={this.onPressHandler}
	        disabled={disabled}
	      />
	      <Button title="Resend code" onPress={this.onPressResend} />
	      {!!resendMessage && <Text style={{ padding: 5 }}>{resendMessage}</Text>}
	      <Button title="Update phone number" onPress={this.onPressUpdatePhone} />
	    </View>
	  );
	}
}

const styles = StyleSheet.create({
  errorMessage: {
    color: 'rgb(199, 0, 57)',
    padding: 5,
  },
});
export default withNavigation(ConfirmAccount);
