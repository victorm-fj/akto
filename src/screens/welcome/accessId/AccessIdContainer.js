import React from 'react';
import { Query } from 'react-apollo';
import AccessId from './AccessId';
import GetCodeType from '../../../queries/GetCodeType';
import { Loading } from '../../../components/chatbot';
import Error from '../../../components/Error';

const AccessIdContainer = (props) => {
  if (
    typeof props.steps.accessId === 'undefined' ||
		typeof props.steps.accessId.value === 'undefined'
  ) {
    setTimeout(
      () => props.triggerNextStep({ value: undefined, trigger: 'accessId' }),
      0
    );
    return null;
  }
  const codeId = props.steps.accessId.value.trim();
  return (
    <Query
      query={GetCodeType}
      variables={{ codeId }}
      fetchPolicy="network-only"
    >
      {({ loading, error, data }) => {
        if (loading) return <Loading color="rgb(199, 0, 57)" />;
        if (error) return <Error message={error} />;
        return <AccessId {...props} codeType={data.getCodeType} />;
      }}
    </Query>
  );
};

export default AccessIdContainer;
