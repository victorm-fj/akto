import React from 'react';

class AccessId extends React.Component {
  componentWillMount() {
    const {
      triggerNextStep,
      codeType: { codeId, username, isInvitation, isUsername },
    } = this.props;
    if (isInvitation) {
      // accessId is a valid code
      // using self flag we set the value of the accessId step to the one received by the api
      // when accessId is an invitationId we receive the codeId/accessId
      // and the new username for the registering user
      const valueObj = { codeId, username };
      const value = JSON.stringify(valueObj);
      triggerNextStep({ value, trigger: '4', self: true });
      setTimeout(
        () => triggerNextStep({ value: undefined, trigger: 'yesNo' }),
        0
      );
    } else if (isUsername) {
      // user is already registered in the app
      triggerNextStep({ value: codeId, trigger: '19' });
      setTimeout(
        () => triggerNextStep({ value: undefined, trigger: 'signIn' }),
        0
      );
    } else {
      triggerNextStep({ value: undefined, trigger: '3' });
      setTimeout(
        () => triggerNextStep({ value: undefined, trigger: 'accessId' }),
        0
      );
    }
  }
  render() {
    return null;
  }
}

export default AccessId;
