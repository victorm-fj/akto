import React from 'react';
import { View, Button, Text, TextInput, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Auth } from 'aws-amplify';
import { validatePassword } from '../utils/helpers';

const requiredPasswordText = 'Please fill out your password!';
const errorPasswordText =
	'Invalid password! Password must contain numbers, at least one special character, uppercase and lowercase letters, and be at least 8 characters long.';

class SignIn extends React.Component {
	state = { password: '', disabled: true, passwordError: '', authError: '' };
	validatePassword = () => {
	  const { password } = this.state;
	  if (!password) {
	    this.setState({ passwordError: requiredPasswordText, disabled: true });
	  } else {
	    if (validatePassword(password)) {
	      this.setState({ passwordError: '', disabled: false });
	    } else {
	      this.setState({ passwordError: errorPasswordText, disabled: true });
	    }
	  }
	};
	onChangePassword = (password) => {
	  this.setState({ password: password.trim() });
	};
	onPressHandler = async () => {
	  const { password } = this.state;
	  const { steps, triggerNextStep, navigation } = this.props;
	  const username = steps.accessId.value.trim();
	  try {
	    await Auth.signIn(username, password);
	    triggerNextStep({ value: undefined, trigger: 'end' });
	    setTimeout(() => navigation.navigate('Main'), 1500);
	  } catch (error) {
	    console.log('signIn error', error);
	    this.setState({ authError: error.message });
	  }
	};
	onPressForgotPassword = async () => {
	  const { steps, triggerNextStep } = this.props;
	  const username = steps.accessId.value.trim();
	  try {
	    await Auth.forgotPassword(username);
	    triggerNextStep({ value: undefined, trigger: 'forgotPassword' });
	  } catch (error) {
	    console.log('ForgotPassword flow error', error);
	    this.setState({ authError: error.message });
	  }
	};
	render() {
	  const { password, disabled, passwordError, authError } = this.state;
	  return (
	    <View style={{ justifyContent: 'center' }}>
	      <TextInput
	        style={{ height: 40 }}
	        onChangeText={this.onChangePassword}
	        placeholder="Password..."
	        value={password}
	        onBlur={this.validatePassword}
	        secureTextEntry
	      />
	      <Text style={styles.errorMessage}>{passwordError}</Text>
	      <Text style={styles.errorMessage}>{authError}</Text>
	      <Button
	        title="Enter"
	        onPress={this.onPressHandler}
	        disabled={disabled}
	      />
	      <Button title="Forgot password" onPress={this.onPressForgotPassword} />
	    </View>
	  );
	}
}

const styles = StyleSheet.create({
  errorMessage: {
    color: 'rgb(199, 0, 57)',
    padding: 5,
  },
});
export default withNavigation(SignIn);
