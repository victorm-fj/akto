import React from 'react';
import { View, Text, Button } from 'react-native';
import ChatBot from '../../components/chatbot';
import steps from './steps';
import { removeData } from '../../components/chatbot/storage';

class Bot extends React.Component {
	state = { botKey: 0 };
	reload = async () => {
	  await removeData('@authBot');
	  this.setState({ botKey: this.state.botKey + 1 });
	};
	render() {
	  return (
	    <View style={{ flex: 1, backgroundColor: '#1F2341' }}>
	      <Text
	        style={{
	          fontSize: 20,
	          fontWeight: 'bold',
	          color: '#fafafa',
	          position: 'absolute',
	          top: 10,
	          left: 10,
	          zIndex: 1000,
	        }}
	      >
					AKTO
	      </Text>
	      <View
	        style={{
	          position: 'absolute',
	          top: 10,
	          right: 10,
	          width: '50%',
	          alignSelf: 'flex-end',
	          zIndex: 1000,
	        }}
	      >
	        <Button title="Reload" color="#fafafa" onPress={this.reload} />
	      </View>
	      <ChatBot
	        key={this.state.botKey}
	        cache
	        cacheName="@authBot"
	        style={{ backgroundColor: 'transparent' }}
	        steps={steps}
	        handleEnd={this.onHandleEnd}
	        contentStyle={{
	          marginTop: 40,
	          paddingBottom: 10,
	          backgroundColor: '#1F2341',
	        }}
	        hideBotAvatar
	        hideUserAvatar
	        bubbleStyle={{ backgroundColor: 'transparent' }}
	        botFontColor="rgb(199, 0, 57)"
	        userFontColor="#fafafa"
	        inputStyle={{ backgroundColor: '#fafafa', color: '#000' }}
	        submitButtonStyle={{ backgroundColor: '#411F3D' }}
	        buttonTextStyle={{ color: '#fafafa', fontWeight: 'bold' }} // Customization
	        locale={this.props.locale}
	      />
	    </View>
	  );
	}
}

export default Bot;
