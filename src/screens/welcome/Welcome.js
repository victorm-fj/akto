import React from 'react';
import { View } from 'react-native';
import Bot from './Bot';

class Welcome extends React.Component {
  render() {
    console.log('Welcome screen props -', this.props);
    return (
      <View style={{ flex: 1, backgroundColor: '#1F2341' }}>
        <Bot locale={this.props.screenProps.currentLocale} />
      </View>
    );
  }
}

export default Welcome;
