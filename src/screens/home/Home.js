import React from 'react';
import { View, Text, Button } from 'react-native';
import { Auth } from 'aws-amplify';
import styles from './styles';
import { strings } from '../../locales/i18n';

class Home extends React.Component {
	onPressHandler = async () => {
	  await Auth.signOut();
	  this.props.navigation.navigate('Auth');
	};
	onOpenARHandler = () => {
	  this.props.navigation.navigate('AR');
	};
	render() {
	  console.log('Homescreen props -', this.props);
	  const { user } = this.props;
	  return (
	    <View style={styles.container}>
	      <Text style={styles.title}>Akto</Text>
	      <Text style={styles.subtitle}>{strings('home.agencySubtitle')}</Text>
	      <Text style={[styles.text, { paddingTop: 10, fontWeight: 'bold' }]}>
	        {strings('home.agentId')}: {user.username}
	      </Text>
	      <Text style={styles.text}>
	        {strings('home.invitationId')}: {user.invitationId}
	      </Text>
	      <Button
	        title={strings('home.signOutAction')}
	        onPress={this.onPressHandler}
	      />
	      <Button title="Open AR" onPress={this.onOpenARHandler} />
	    </View>
	  );
	}
}

export default Home;
