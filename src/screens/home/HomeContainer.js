import React from 'react';
import { Query } from 'react-apollo';
import Home from './Home';
import GetUser from '../../queries/GetUser';
import Loader from '../../components/Loader';
import Error from '../../components/Error';

const HomeContainer = (props) => {
  return (
    <Query query={GetUser} fetchPolicy="network-only">
      {({ loading, error, data }) => {
        if (loading) return <Loader />;
        if (error) {
          console.log('HomeContainer error -', error);
          return <Error message={error} />;
        }
        return <Home {...props} user={data.getUser} />;
      }}
    </Query>
  );
};

export default HomeContainer;
