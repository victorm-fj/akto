import React from 'react';
import { View, Text, Button } from 'react-native';
import styles from '../styles';
import Typing from './Typing';
import PhoneNumber from '../input/PhoneNumber';
class MessageItem extends React.Component {
  render() {
    const { item } = this.props;
    const botMessage = item.from === 'bot';
    const messageStyle = botMessage ? styles.botMessage : styles.userMessage;
    const textStyle = botMessage ? styles.botText : styles.userText;
    if (botMessage && item.message === 'typing') {
      return (
        <View style={[styles.message, messageStyle]}>
          <Typing />
        </View>
      );
    }
    if (item.type === 'phoneNumber') {
      return (
        <View>
          <View style={[styles.message, messageStyle]}>
            <Text style={textStyle}>{item.message}</Text>
          </View>
          <PhoneNumber />
        </View>
      );
    }
    if (item.actions) {
      return (
        <View>
          <View style={[styles.message, messageStyle]}>
            <Text style={textStyle}>{item.message}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            {item.actions.map((action) => {
              return (
                <View key={action} style={{ paddingRight: 10, paddingTop: 10 }}>
                  <Button
                    title={action}
                    onPress={() => this.props.sendMessage(action)}
                  />
                </View>
              );
            })}
          </View>
        </View>
      );
    }
    return (
      <View style={[styles.message, messageStyle]}>
        <Text style={textStyle}>{item.message}</Text>
      </View>
    );
  }
}

export default MessageItem;
