export const validatePassword = (password) => {
  return (
    /[A-Z]/.test(password) &&
		/[a-z]/.test(password) &&
		/[0-9]/.test(password) &&
		/[^A-Za-z0-9]/.test(password) &&
		password.length > 7
  );
};

export const validityText =
	'Please enter a secure password.\n\nPassword must contain numbers, at least one special character, uppercase and lowercase letters, and be at least 8 characters long.';
