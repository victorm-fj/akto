import React from 'react';
import { Text, FlatList, KeyboardAvoidingView } from 'react-native';
import styles from './styles';
import MessageItem from './message/MessageItem';
import InputContainer from './input/InputContainer';
import { validatePassword, validityText } from './helpers';
class Onboard extends React.Component {
	state = {
	  userInput: '',
	};
	componentDidMount() {
	  this.props.initOnboard();
	}
	createUser(password) {
	  const {
	    recruitId,
	    phoneNumber,
	    firstName,
	    lastName,
	  } = this.props.onboard.sessionAttributes;
	  this.props.createUser(
	    recruitId,
	    password,
	    phoneNumber,
	    firstName,
	    lastName
	  );
	}
	handleTextSubmit = () => {
	  const { sessionAttributes } = this.props.onboard;
	  const inputText = this.state.userInput.trim();
	  this.setState({ userInput: '' });
	  if (inputText !== '') {
	    if (sessionAttributes.phoneNumber) {
	      const userMessage = { from: 'user', message: inputText, type: '' };
	      this.props.addMessage(userMessage);
	      this.validatePassword(inputText);
	    } else {
	      this.props.sendMessage(inputText, this.list);
	    }
	  }
	};
	onChangeText = (text) => this.setState({ userInput: text });
	validatePassword(password) {
	  if (validatePassword(password)) {
	    this.createUser(password);
	  } else {
	    const message = { from: 'bot', message: validityText };
	    this.props.addMessage(message, this.list);
	  }
	}
	renderItem = ({ item }) => {
	  return <MessageItem item={item} />;
	};
	render() {
	  const { userInput } = this.state;
	  const { inputEditable, messages } = this.props.onboard;
	  return (
	    <KeyboardAvoidingView style={styles.container}>
	      <Text style={styles.topTitle}>AKTO</Text>
	      <FlatList
	        ref={(list) => (this.list = list)}
	        data={messages}
	        renderItem={this.renderItem}
	        keyExtractor={(item, index) => String(index)}
	        extraData={messages}
	        style={styles.list}
	        onContentSizeChange={() => this.list.scrollToEnd({ animated: true })}
	      />
	      <InputContainer
	        userInput={userInput}
	        inputEditable={inputEditable}
	        onChangeText={this.onChangeText}
	        handleTextSubmit={this.handleTextSubmit}
	        isRecording={false}
	      />
	    </KeyboardAvoidingView>
	  );
	}
}

export default Onboard;
