import React from 'react';
import { View, TextInput, Button } from 'react-native';
import styles from './styles';

const InputContainer = ({
  onChangeText,
  userInput,
  inputEditable,
  handleTextSubmit,
  isRecording,
}) => {
  return (
    <View style={styles.inputContainer}>
      <Button
        title={isRecording ? 'Stop' : 'Record'}
        color="#411F23"
        onPress={() => {}}
      />
      <TextInput
        onChangeText={onChangeText}
        value={userInput}
        style={styles.textInput}
        editable={inputEditable}
        placeholder="Type here to talk..."
        placeholderTextColor="#fafafa"
        underlineColorAndroid="#fafafa"
      />
      <Button
        title="Send"
        color="#413D1F"
        style={styles.send}
        onPress={handleTextSubmit}
      />
    </View>
  );
};

export default InputContainer;
