import React from 'react';
import { View, Button, Alert } from 'react-native';
import PhoneInput from 'react-native-phone-input';
// import CountryModalPicker from './modal/CountryModalPicker';
// import styles from './styles';
class PhoneNumber extends React.Component {
	state = { pickerData: null };
	componentDidMount() {
	  this.setState({ pickerData: this.phone.getPickerData() });
	  this.phone.focus();
	}
	// onPressFlag = () => {
	//   this.countryPicker.open();
	// };
	// selectCountry = (country) => {
	//   this.phone.selectCountry(country.iso2);
	// };
	onPressHandler = () => {
	  const validNumber = this.phone.isValidNumber();
	  const phoneNumer = this.phone.getValue();
	  console.log(validNumber, phoneNumer);
	  if (validNumber) {
	    this.props.sendMessage(phoneNumer);
	  } else {
	    const validationMessage = 'Please enter a valid phone number.';
	    Alert.alert('Invalid phone number', validationMessage, [
	      { text: 'OK', onPress: () => {} },
	    ]);
	  }
	};
	render() {
	  return (
	  // <View style={styles.phoneInput}>
	    <View
	      style={{
	        width: '70%',
	        height: 100,
	        justifyContent: 'space-around',
	      }}
	    >
	      <PhoneInput
	        ref={(phone) => (this.phone = phone)}
	        textStyle={{ color: '#fafafa' }}
	        // onPressFlag={this.onPressFlag}
	      />
	      <Button
	        title="Send"
	        onPress={this.onPressHandler}
	        style={{ alignSelf: 'flex-end' }}
	      />
	    </View>
	  /*<CountryModalPicker
	        ref={(countryPicker) => (this.countryPicker = countryPicker)}
	        data={this.state.pickerData}
	        onChange={this.selectCountry}
	        cancelText="Cancel"
	      /> */
	  // </View>
	  );
	}
}

export default PhoneNumber;
