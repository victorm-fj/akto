import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1F2341',
  },
  topTitle: {
    fontSize: 20,
    color: '#fafafa',
    position: 'absolute',
    top: 10,
    left: 10,
  },
  list: {
    flex: 1,
    width: '100%',
    paddingHorizontal: 40,
    marginTop: 54,
  },
  message: {
    marginVertical: 5,
    flex: 1,
    flexDirection: 'row',
  },
  botMessage: {},
  userMessage: {
    alignSelf: 'flex-end',
  },
  botText: {
    color: 'rgb(199, 0, 57)',
    fontSize: 16,
  },
  userText: {
    color: '#fafafa',
    fontSize: 16,
  },
  ellipsis: {
    fontSize: 18,
    color: 'rgb(199, 0, 57)',
  },
});

export default styles;
