import React from 'react';
import { StyleSheet, View, FlatList, Alert } from 'react-native';
import { List, ListItem, CheckBox, Button } from 'react-native-elements';
import CountryPicker from 'react-native-country-picker-modal';
import CarrierInfo from 'react-native-carrier-info';
import { getContactsList } from './helpers';
import { Loading } from '../../../../../components/chatbot';
import { getCountry } from '../../../../../utils/countriesInfo';

class ContactsComponent extends React.Component {
	state = {
	  invites: [],
	  contacts: [],
	  userCountry: '',
	  userCallingCode: '',
	  loading: true,
	};
	async componentWillMount() {
	  try {
	    const userCountry = await CarrierInfo.isoCountryCode();
	    const userCallingCode = getCountry(userCountry).dial;
	    console.log(
	      'userCountry',
	      userCountry,
	      'userCallingCode',
	      userCallingCode
	    );
	    this.setState({
	      userCountry: userCountry.toUpperCase(),
	      userCallingCode,
	    });
	  } catch (error) {
	    console.log('isoCountryCode error', error);
	    this.setState({ userCountry: 'US', userCallingCode: '1' });
	  }
	}
	async componentWillReceiveProps(nextProps) {
	  const permission = this.props.permissions[0];
	  if (
	    this.state.contacts.length === 0 &&
			nextProps.permissionsStatus[permission] === 'authorized'
	  ) {
	    try {
	      const contactsList = await getContactsList();
	      const contacts = contactsList.map((contact) => ({
	        ...contact,
	        callingCode: this.state.userCallingCode,
	        country: this.state.userCountry,
	      }));
	      this.setState({ contacts });
	      setTimeout(() => this.setState({ loading: false }), 1500);
	    } catch (error) {
	      console.log('getting contacts error', error);
	      Alert.alert(
	        'Error',
	        'Something went wrong, can not get contacts list',
	        [
	          {
	            text: 'OK',
	            onPress: () => {
	              console.log('OK Pressed');
	              this.onPressClose();
	            },
	          },
	        ]
	      );
	    }
	  }
	}
	selectCountry = (country, recordID) => {
	  console.log('selectCountry', country, 'recordID', recordID);
	  this.setState((prevState) => {
	    const updatedContacts = prevState.contacts.map((contact) => {
	      if (contact.recordID === recordID) {
	        return {
	          ...contact,
	          callingCode: country.callingCode,
	          country: country.cca2,
	        };
	      }
	      return contact;
	    });
	    return { contacts: [...updatedContacts] };
	  });
	};
	onPressClose = () => {
	  const { triggerNextStep } = this.props;
	  triggerNextStep({ value: undefined, trigger: 'end' });
	};
	onPressCheckBox = (recordID) => {
	  console.log('recordID', recordID);
	  this.setState((prevState) => {
	    const recordIDIndex = prevState.invites.indexOf(recordID);
	    if (recordIDIndex < 0) {
	      return { invites: [...prevState.invites, recordID] };
	    }
	    const filteredInvites = prevState.invites.filter(
	      (invite) => invite !== recordID
	    );
	    return { invites: [...filteredInvites] };
	  });
	};
	onPressinviteContacts = async () => {
	  const { contacts, invites } = this.state;
	  const recruits = contacts
	    .filter((contact) => invites.indexOf(contact.recordID) >= 0)
	    .map(({ recordID, phoneNumbers, callingCode }) => ({
	      recordId: recordID,
	      phoneNumber: `+${callingCode}${phoneNumbers[0].number.replace(
	        /\D/g,
	        ''
	      )}`,
	    }));
	  console.log('recruits', recruits);
	  try {
	    const data = await this.props.onInviteRecruits(recruits);
	    console.log('got data', data);
	  } catch (error) {
	    console.log('something went wrong', error);
	  }
	  this.onPressClose();
	};
	renderItem = ({ item }) => {
	  const {
	    recordID,
	    givenName,
	    middleName,
	    familyName,
	    phoneNumbers,
	    country,
	  } = item;
	  const checked = this.state.invites.indexOf(recordID) < 0 ? false : true;
	  return (
	    <ListItem
	      key={recordID}
	      title={`${givenName || ''} ${middleName || ''} ${familyName || ''}`}
	      subtitle={phoneNumbers.length > 0 ? phoneNumbers[0].number : ''}
	      leftIcon={
	        <CountryPicker
	          ref={(countryPicker) => (this.countryPicker = countryPicker)}
	          onChange={(country) => this.selectCountry(country, recordID)}
	          cca2={country}
	          onClose={() => {}}
	        />
	      }
	      rightIcon={
	        <CheckBox
	          title="Invite"
	          checked={checked}
	          onPress={() => this.onPressCheckBox(recordID)}
	        />
	      }
	    />
	  );
	};
	render() {
	  console.log('contacts state', this.state);
	  const { invites, contacts, loading } = this.state;
	  if (loading) {
	    return (
	      <View
	        style={{
	          display: 'flex',
	          alignItems: 'center',
	        }}
	      >
	        <Loading color="#000" />
	      </View>
	    );
	  }
	  return (
	    <View style={styles.container}>
	      <List containerStyle={{ marginBottom: 20 }}>
	        <FlatList
	          data={this.state.contacts}
	          renderItem={this.renderItem}
	          extraData={this.state}
	          keyExtractor={(item, index) => String(index)}
	        />
	      </List>
	      {invites.length > 0 ? (
	        <Button
	          title="Send invitations"
	          onPress={this.onPressinviteContacts}
	          buttonStyle={{ backgroundColor: '#2089dc' }}
	        />
	      ) : (
	        <Button
	          title="Close"
	          onPress={this.onPressClose}
	          buttonStyle={{ backgroundColor: '#FF190C' }}
	          disabled={contacts.length === 0}
	        />
	      )}
	    </View>
	  );
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    fontSize: 18,
  },
});
export default ContactsComponent;
