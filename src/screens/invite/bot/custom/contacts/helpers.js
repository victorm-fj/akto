import Contacts from 'react-native-contacts';

export const getContactsList = () =>
  new Promise((resolve, reject) => {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        reject(err);
        return;
      }
      resolve(contacts);
    });
  });
