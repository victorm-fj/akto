import { graphql } from 'react-apollo';
import Contacts from './Contacts';
import InviteRecruits from '../../../../../mutations/InviteRecruits';
import requirePermissions from '../../../../../components/requirePermissions';

const permissions = ['contacts'];

export default graphql(InviteRecruits, {
  options: {
    fetchPolicy: 'no-cache', // Error: fetchPolicy for mutations currently only supports the 'no-cache' policy
  },
  props: ({ ownProps, mutate }) => ({
    onInviteRecruits: (recruits) =>
      mutate({
        variables: {
          recruits,
        },
        optimisticResponse: {
          __typename: 'Mutation',
          inviteRecruits: recruits.map((recruit) => ({
            __typename: 'RecruitType',
            recordId: recruit.recordId,
            phoneNumber: recruit.phoneNumber,
          })),
        },
      }),
  }),
})(requirePermissions(Contacts, permissions));
