import React from 'react';
import { View, Button } from 'react-native';
import ChatBot from '../../../components/chatbot';
import steps from './steps';
import { removeData } from '../../../components/chatbot/storage';

class InviteBot extends React.Component {
	state = { botKey: 0 };
	render() {
	  return (
	    <View style={{ flex: 1, backgroundColor: '#1F2341' }}>
	      <View
	        style={{
	          position: 'absolute',
	          top: 10,
	          right: 10,
	          width: '50%',
	          alignSelf: 'flex-end',
	          zIndex: 1000,
	        }}
	      >
	        <Button
	          title="Reload"
	          color="#fafafa"
	          onPress={() => {
	            removeData('@inviteBot');
	            this.setState({ botKey: this.state.botKey + 1 });
	          }}
	        />
	      </View>

	      <ChatBot
	        key={this.state.botKey}
	        cache
	        cacheName="@inviteBot"
	        style={{ backgroundColor: 'transparent' }}
	        steps={steps}
	        contentStyle={{
	          marginTop: 40,
	          paddingBottom: 10,
	          backgroundColor: '#1F2341',
	        }}
	        hideBotAvatar
	        hideUserAvatar
	        bubbleStyle={{ backgroundColor: 'transparent' }}
	        botFontColor="rgb(199, 0, 57)"
	        userFontColor="#fafafa"
	        inputStyle={{ backgroundColor: '#fafafa', color: '#000' }}
	        submitButtonStyle={{ backgroundColor: '#411F3D' }}
	        buttonTextStyle={{ color: '#fafafa', fontWeight: 'bold' }} // Customization
	        locale={this.props.locale}
	      />
	    </View>
	  );
	}
}

export default InviteBot;
