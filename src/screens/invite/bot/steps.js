import React from 'react';
import ContactsContainer from './custom/contacts/ContactsContainer';
import { strings } from '../../../locales/i18n';

export default [
  {
    id: '1',
    message: strings('invite.hi'),
    trigger: 'initialOptions',
  },
  {
    id: 'initialOptions',
    options: [
      {
        value: 'invite',
        label: strings('invite.optionInvite'),
        trigger: 'getContacts',
      },
      {
        value: 'nothing',
        label: strings('invite.optionNothing'),
        trigger: 'end',
      },
    ],
  },
  {
    id: 'getContacts',
    component: <ContactsContainer />,
    replace: true,
    waitAction: true,
  },
  {
    id: 'end',
    message: strings('invite.end'),
    end: true,
  },
];
