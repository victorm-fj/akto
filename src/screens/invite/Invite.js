import React from 'react';
import { View } from 'react-native';
import InviteBot from './bot/InviteBot';

class Invite extends React.Component {
  render() {
    console.log('Invite screen props -', this.props);
    return (
      <View style={{ flex: 1, backgroundColor: '#1F2341' }}>
        <InviteBot locale={this.props.screenProps.currentLocale} />
      </View>
    );
  }
}

export default Invite;
