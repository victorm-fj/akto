import React from 'react';
import { StyleSheet } from 'react-native';
import {
  ViroARScene,
  ViroText,
  ViroMaterials,
  ViroBox,
  ViroQuad,
  Viro3DObject,
  ViroController,
  ViroNode,
  ViroARPlane,
  ViroFlexView,
  ViroAmbientLight,
  ViroLightingEnvironment,
} from 'react-viro';

const CONTROLLER_PUSH = 1;
const CONTROLLER_GRIP = 2;
const CONTROLLER_PULL = 3;

class PhysicsSample extends React.Component {
	state = {
	  controllerConfig: CONTROLLER_GRIP,
	  showCollisionBox: false,
	  foundPlane: false,
	  planePosition: [0, 0, 0],
	  planeRotation: [0, 0, 0],
	  totalCubes: 0,
	};
	/*
   When the anchor plane is found, set our physics example at the location of the plane.
   And then freeze the plane at that location to prevent it from re-adjusting to updates.
   */
	onAnchorFound = (anchor) => {
	  const { position, center, rotation, type } = anchor;
	  if (type !== 'plane') {
	    return;
	  }
	  const worldCenterPosition = position.map((pos, i) => pos + center[i]);
	  this.arPlane.setNativeProps({ pauseUpdates: true });
	  this.setState({
	    foundPlane: true,
	    planePosition: worldCenterPosition,
	    planeRotation: rotation,
	  });
	};
	getPhysicsGroup() {
	  const { foundPlane, planePosition, controllerConfig } = this.state;
	  if (!foundPlane) {
	    return;
	  }
	  this.ballProperties = {
	    friction: 0.6,
	    type: 'Dynamic',
	    mass: 4,
	    enabled: true,
	    useGravity: true,
	    shape: { type: 'Sphere', params: [0.14] },
	    restitution: 0.65,
	  };
	  return (
	    <ViroNode position={planePosition}>
	      {this.getHUDControl()}
	      <ViroController ref={(controller) => (this.controller = controller)} />
	      <Viro3DObject
	        ref={(ball) => (this.ball = ball)}
	        scale={[0.5, 0.5, 0.5]}
	        position={[0, 0, 1.0]}
	        rotation={[0, 0, 0]}
	        source={require('./res/object_basketball_pbr.vrx')}
	        resources={[
	          require('./res/blinn1_Base_Color.png'),
	          require('./res/blinn1_Metallic.png'),
	          require('./res/blinn1_Roughness.png'),
	          require('./res/blinn1_Normal_OpenGL.png'),
	        ]}
	        type="VRX"
	        physicsBody={this.ballProperties}
	        viroTag="BallTag"
	        onClick={
	          controllerConfig == CONTROLLER_PUSH
	            ? this.onItemPushImpulse
	            : undefined
	        }
	        onDrag={controllerConfig === CONTROLLER_GRIP ? () => {} : undefined}
	      />
	      {this.renderCubes()}
	      <ViroQuad
	        position={[0, 0, 0]}
	        scale={[6.0, 8.0, 1.0]}
	        rotation={[-90, 0, 0]}
	        pyshicsBody={{ type: 'Static', restitution: 0.75 }}
	        onClickState={
	          controllerConfig == CONTROLLER_PULL
	            ? this.onItemPullForce
	            : undefined
	        }
	        ref={(floorSurface) => (this.floorSurface = floorSurface)}
	        onCollision={this.onFloorCollide}
	        materials="ground"
	      />
	    </ViroNode>
	  );
	}
	onItemPushImpulse = (position, source) => {
	  console.log('onItemPushImpulse', position, source);
	  // return (position, source) => {
	  //   this.controllerRef.getControllerForwardAsync().then((forward) => {
	  //     var pushStrength = 3;
	  //     var pushImpulse = [
	  //       forward[0] * pushStrength,
	  //       forward[1] * pushStrength,
	  //       forward[2] * pushStrength,
	  //     ];
	  //     this.ball.getTransformAsync().then((transform) => {
	  //       var pos = transform.position;
	  //       var pushPosition = [
	  //         position[0] - pos[0],
	  //         position[1] - pos[1],
	  //         position[2] - pos[2],
	  //       ];
	  //       this.ball.applyImpulse(pushImpulse, pushPosition);
	  //     });
	  //   });
	  // };
	};
	getHUDControl() {
	  return (
	    <ViroNode
	      position={[0, 1.5, -7.75]}
	      transformBehaviors={['billboardX', 'billboardY']}
	    >
	      <ViroFlexView
	        style={{ flexDirection: 'column' }}
	        width={1}
	        height={0.8}
	        materials="hud_text_bg"
	        position={[-1.5, 0, 0]}
	        onClick={this.resetScene}
	      >
	        <ViroText style={styles.text} text="Reset Scene" />
	      </ViroFlexView>

	      <ViroFlexView
	        style={{ flexDirection: 'column' }}
	        width={1}
	        height={0.8}
	        materials="hud_text_bg"
	        position={[0, 0, 0]}
	        onClick={this.toggleControllerInteraction}
	      >
	        <ViroText
	          style={styles.hud_text}
	          text={this.getControllerTextMode()}
	        />
	      </ViroFlexView>

	      <ViroFlexView
	        style={{ flexDirection: 'column' }}
	        width={1}
	        height={0.8}
	        materials="hud_text_bg"
	        position={[1.5, 0, 0]}
	        onClick={this.toggleCollisionBox}
	      >
	        <ViroText style={styles.hud_text} text="Toggle Phyz boxes" />
	      </ViroFlexView>

	      <ViroFlexView
	        style={{ flexDirection: 'column' }}
	        width={1}
	        height={0.8}
	        materials="hud_text_bg"
	        position={[3, 0, 0]}
	        onClick={this.addCube}
	      >
	        <ViroText style={styles.text} text="Add Cube" />
	      </ViroFlexView>
	    </ViroNode>
	  );
	}
	getControllerTextMode = () => {
	  if (this.state.controllerConfig == 1) {
	    return 'Controller Push Mode';
	  } else if (this.state.controllerConfig == 2) {
	    return 'Controller Drag Mode';
	  } else {
	    return 'Controller Pull Mode';
	  }
	};
	resetScene = () => {
	  // Reset the ball to it's default position.
	  setTimeout(() => {
	    this.ball.setNativeProps({ physicsBody: null });
	    this.ball.setNativeProps({ position: [0, 1, -0.3] });
	    this.floorSurface.setNativeProps({ materials: ['ground'] });

	    setTimeout(() => {
	      this.ball.setNativeProps({ physicsBody: this.ballProperties });
	    }, 500);
	  }, 500);
	};
	toggleCollisionBox = () => {
	  this.setState({ showCollisionBox: !this.state.showCollisionBox });
	};
	toggleControllerInteraction = () => {
	  var endConfig = 1;
	  if (this.state.controllerConfig == 1) {
	    endConfig = 2;
	  } else if (this.state.controllerConfig == 2) {
	    endConfig = 3;
	  }
	  this.setState({ controllerConfig: endConfig });
	};
	onItemPullForce = (itemTag) => {
	  return (state, position, source) => {
	    this.sceneRef.getCameraOrientationAsync().then((camTransform) => {
	      this.ball.getTransformAsync().then((ballTransform) => {
	        var ballPos = ballTransform.position;
	        var camPos = camTransform.position;
	        var pullVec = [
	          camPos[0] - ballPos[0],
	          camPos[1] - ballPos[1],
	          camPos[2] - ballPos[2],
	        ];

	        var pullStrength = 5; // Force multiplier.
	        var pullStrengthVec = [
	          pullVec[0] * pullStrength,
	          pullVec[1] * pullStrength,
	          pullVec[2] * pullStrength,
	        ]; // Force in newtons
	        let phyzProps = {};
	        if (state == 1) {
	          phyzProps = {
	            force: { value: pullStrengthVec },
	            type: 'Dynamic',
	            mass: 4,
	            enabled: true,
	            useGravity: true,
	            shape: { type: 'Sphere', params: [0.14] },
	            restitution: 0.65,
	          };
	        } else {
	          phyzProps = {
	            type: 'Dynamic',
	            mass: 4,
	            enabled: true,
	            useGravity: true,
	            shape: { type: 'Sphere', params: [0.14] },
	            restitution: 0.65,
	          };
	        }
	        this.ball.setNativeProps({ physicsBody: phyzProps });
	      });
	    });
	  };
	};
	onFloorCollide = (collidedTag, collidedPoint, collidedNormal) => {
	  console.log('Viro box has collided on the ' + collidedTag);
	  if (collidedTag == 'BallTag') {
	    this.floorSurface.setNativeProps({ materials: ['ground_hit'] });
	  }
	};
	renderCubes() {
	  var views = [];
	  for (var i = 0; i < this.state.totalCubes; i++) {
	    var cubeKey = 'CubeTag_' + i;
	    views.push(
	      <ViroBox
	        scale={[0.2, 0.2, 0.2]}
	        position={[-0.5, 1, -1.3]}
	        rotation={[0, 0, 0]}
	        physicsBody={{
	          type: 'Dynamic',
	          mass: 25,
	          enabled: true,
	          useGravity: true,
	          restitution: 0.35,
	          friction: 0.75,
	        }}
	        materials="cube_color"
	        key={cubeKey}
	        onClick={
	          this.state.controllerConfig == CONTROLLER_PUSH
	            ? this.onItemPushImpulse(cubeKey)
	            : undefined
	        }
	        onDrag={
	          this.state.controllerConfig == CONTROLLER_GRIP
	            ? () => {}
	            : undefined
	        }
	      />
	    );
	  }
	  return views;
	}
	addCube = () => {
	  this.setState({ totalCubes: this.state.totalCubes + 1 });
	};
	render() {
	  return (
	    <ViroARScene
	      physicsWorld={{
	        gravity: [0, -9.81, 0],
	        drawBounds: this.state.showCollisionBox,
	      }}
	      ref={(arScene) => (this.arScene = arScene)}
	    >
	      <ViroAmbientLight color="#ffffff" intensity={10} />
	      <ViroLightingEnvironment source={require('./res/ibl_envr.hdr')} />
	      <ViroARPlane
	        key="firstPlane"
	        ref={(arPlane) => (this.arPlane = arPlane)}
	        onAnchorFound={this.onAnchorFound}
	      >
	        {this.getPhysicsGroup()}
	      </ViroARPlane>
	    </ViroARScene>
	  );
	}
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    color: '#00a',
    flex: 1,
  },
});
ViroMaterials.createMaterials({
  hud_text_bg: {
    diffuseColor: '#00ffff',
  },
  ground: {
    diffuseColor: '#007CB6E6',
  },
  ground_hit: {
    diffuseColor: '#008141E6',
  },
  cube_color: {
    diffuseColor: '#0021cbE6',
  },
});
export default PhysicsSample;
