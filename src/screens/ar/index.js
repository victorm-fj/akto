import React from 'react';
import { ViroARSceneNavigator } from 'react-viro';
import MainScene from './MainScene';
import PhysicsSample from './PhysicsSample';

class AR extends React.Component {
  render() {
    return (
      <ViroARSceneNavigator
        apiKey="4C580DD6-221E-4DB7-BC2C-C02D8CA25F18"
        initialScene={{ scene: MainScene }}
      />
    );
  }
}

export default AR;
