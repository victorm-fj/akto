import React from 'react';
import { StyleSheet, Alert, Platform } from 'react-native';
import {
  ViroARScene,
  ViroConstants,
  Viro3DObject,
  ViroAmbientLight,
  ViroPortalScene,
  ViroPortal,
  Viro360Image,
  ViroARPlaneSelector,
  ViroText,
  ViroSpotLight,
  ViroNode,
  ViroQuad,
} from 'react-viro';

import portal from './res/portals/picture_frame/portal_picture_frame.vrx';
import frameDiffuse from './res/portals/picture_frame/portal_picture_frame_diffuse.png';
import frameNormal from './res/portals/picture_frame/portal_picture_frame_normal.png';
import frameSpecular from './res/portals/picture_frame/portal_picture_frame_specular.png';
import fireplace from './res/images/fireplace_2k.hdr';

class MainScene extends React.Component {
	state = { initializing: true };
	onTrackingUpdated = (state, reason) => {
	  if (state === ViroConstants.TRACKING_LIMITED) {
	    // Tracking is available, but the camera's position in the
	    // world may be innacurate and should not be used with confidence
	    if (Platform.OS === 'ios') {
	      if (reason === ViroConstants.TRACKING_REASON_EXCESSIVE_MOTION) {
	        // The device is moving too fast for accurate position tracking
	        Alert.alert(
	          'Unable to track position.',
	          'Try moving your phone slower.',
	          [{ text: 'Ok', onPress: () => {} }]
	        );
	      } else if (
	        reason === ViroConstants.TRACKING_REASON_INSUFFICIENT_FEATURES
	      ) {
	        // The scene visible to the camera does not contain enough
	        // distinguishable features for optimal position tracking
	        Alert.alert(
	          'Insuficient features',
	          'Try turning on more lights and moving around.',
	          [{ text: 'Ok', onPress: () => {} }]
	        );
	      }
	    }
	  } else if (state === ViroConstants.TRACKING_UNAVAILABLE) {
	    // Tracking is unavailable: the camera's position in the world is not known.
	    // Propmt the user to move around
	    Alert.alert(
	      'Unable to find a plane.',
	      'Unable to find a surface. Try moving to the side or repositioning your phone.',
	      [{ text: 'Ok', onPress: () => {} }]
	    );
	  } else {
	    this.setState({ initializing: false });
	  }
	};
	render() {
	  console.log('MainScene AR props -', this.props);
	  return (
	    <ViroARScene onTrackingUpdated={this.onTrackingUpdated}>
	      <ViroAmbientLight color="#ffffff" />
	      {this.state.initializing && (
	        <ViroText
	          text="Initializing AR experience..."
	          scale={[0.5, 0.5, 0.5]}
	          position={[0, 0, -1]}
	          style={styles.text}
	        />
	      )}
	      <ViroPortalScene passable dragType="FixedDistance" onDrag={() => {}}>
	        <ViroPortal position={[0, 0, -1]} scale={[0.1, 0.1, 0.1]}>
	          <Viro3DObject
	            source={portal}
	            resources={[frameDiffuse, frameNormal, frameSpecular]}
	            type="VRX"
	          />
	        </ViroPortal>
	        <Viro360Image source={fireplace} />
	      </ViroPortalScene>
	      <ViroARPlaneSelector>
	        <Viro3DObject
	          scale={[0.1, 0.1, 0.1]}
	          position={[0, -0.2, 0]}
	          source={require('./res/objects/terminator/T810.vrx')}
	          resources={[
	            require('./res/objects/terminator/mat0_c.jpg'),
	            require('./res/objects/terminator/mat0_n.png'),
	            require('./res/objects/terminator/mat0_r.jpg'),
	          ]}
	          rotation={[0, 90, 90]}
	          type="VRX"
	          onDrag={() => {}}
	          dragType="FixedDistance"
	        />
	      </ViroARPlaneSelector>
	      <ViroNode
	        position={[0, -1, -1]}
	        dragType="FixedToWorld"
	        onDrag={() => {}}
	      >
	        <ViroSpotLight
	          innerAngle={5}
	          outerAngle={25}
	          direction={[0, -1, 0]}
	          position={[0, 5, 0]}
	          color="#ffffff"
	          castsShadow={true}
	          shadowMapSize={2048}
	          shadowNearZ={2}
	          shadowFarZ={7}
	          shadowOpacity={0.7}
	        />

	        <Viro3DObject
	          source={require('./res/viro_object_lamp/object_lamp.vrx')}
	          resources={[
	            require('./res/viro_object_lamp/object_lamp_diffuse.png'),
	            require('./res/viro_object_lamp/object_lamp_normal.png'),
	            require('./res/viro_object_lamp/object_lamp_specular.png'),
	          ]}
	          position={[0, 0, 0]}
	          scale={[0.3, 0.3, 0.3]}
	          type="VRX"
	        />
	        <ViroQuad
	          rotation={[-90, 0, 0]}
	          position={[0, -0.001, 0]}
	          width={2.5}
	          height={2.5}
	          arShadowReceiver={true}
	        />
	      </ViroNode>
	    </ViroARScene>
	  );
	}
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    color: '#ffffff',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
});
export default MainScene;
