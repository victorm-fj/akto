import React from 'react';
import AWSAppSyncClient from 'aws-appsync';
import { Rehydrated } from 'aws-appsync-react';
import { ApolloProvider } from 'react-apollo';
import Amplify, { Auth } from 'aws-amplify';
import awsmobile from './aws-exports';
import AppSync from './AppSync';
import AppNav from './nav/AppNav';

// Amplify init
Amplify.configure(awsmobile);
// AppSync client instantiation
const client = new AWSAppSyncClient({
  url: AppSync.graphqlEndpoint,
  region: AppSync.region,
  auth: {
    type: AppSync.authenticationType,
    // API_KEY
    // apiKey: AppSync.apiKey,
    // AWS_IAM
    credentials: () => Auth.currentCredentials(),
    // COGNITO USER POOLS
    // jwtToken: async () =>
    //   (await Auth.currentSession()).getAccessToken().getJwtToken(),
  },
  // complexObjectsCredentials: () => Auth.currentCredentials(),
});

class App extends React.Component {
  async componentWillMount() {
    client.initQueryManager();
    await client.resetStore();
  }
  render() {
    return (
      <ApolloProvider client={client}>
        <Rehydrated>
          <AppNav />
        </Rehydrated>
      </ApolloProvider>
    );
  }
}

export default App;
