import gql from 'graphql-tag';

export default gql`
	query GetCodeType($codeId: String!) {
		getCodeType(codeId: $codeId) {
			__typename
			codeId
			username
			isInvitation
			isUsername
		}
	}
`;
