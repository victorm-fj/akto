import gql from 'graphql-tag';

export default gql`
	query GetUser {
		getUser {
			__typename
			userId
			username
			invitationId
			recruiter
			recruits
			language
		}
	}
`;
