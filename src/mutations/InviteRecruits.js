import gql from 'graphql-tag';

export default gql`
	mutation InviteRecruits($recruits: [RecruitInput!]!) {
		inviteRecruits(recruits: $recruits) {
			__typename
			recordId
			phoneNumber
		}
	}
`;
