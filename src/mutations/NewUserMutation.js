import gql from 'graphql-tag';

export default gql`
	mutation NewUser($username: String!, $invitation: String!) {
		newUser(username: $username, invitation: $invitation) {
			__typename
			userId
			username
			invitationId
			recruiter
			recruits
		}
	}
`;
