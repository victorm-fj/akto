import { TabNavigator } from 'react-navigation';
import HomeNav from './HomeNav';
import Invite from '../screens/invite/Invite';

export default TabNavigator({
  Home: HomeNav,
  Invite: { screen: Invite },
});
