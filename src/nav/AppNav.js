import React from 'react';
import { SwitchNavigator } from 'react-navigation';
import I18n from 'react-native-i18n';
import Loading from './Loading';
import AuthNav from './AuthNav';
import MainNav from './MainNav';

const SwitchNav = SwitchNavigator(
  {
    Loading: Loading,
    Auth: AuthNav,
    Main: MainNav,
  },
  {
    initialRouteName: 'Loading',
  }
);

const AppNav = () => {
  const currentLocale = I18n.currentLocale();
  console.log('currentLocale', currentLocale);
  return <SwitchNav screenProps={{ currentLocale }} />;
};

export default AppNav;
