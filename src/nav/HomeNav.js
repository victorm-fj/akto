import { StackNavigator } from 'react-navigation';
import Home from '../screens/home/HomeContainer';
import AR from '../screens/ar';

export default StackNavigator(
  {
    Home: { screen: Home },
    AR: { screen: AR },
  },
  {
    headerMode: 'none',
  }
);
