import React from 'react';
import { Auth } from 'aws-amplify';
import SplashScreen from 'react-native-splash-screen';

class Loading extends React.Component {
  componentWillMount() {
    this.checkUser();
  }
  async checkUser() {
    try {
      const user = await Auth.currentAuthenticatedUser();
      console.log('Authenticated user -', user);
      this.props.navigation.navigate('Main');
    } catch (error) {
      console.log('User is not authenticated -', error);
      this.props.navigation.navigate('Auth');
    } finally {
      SplashScreen.hide();
    }
  }
  render() {
    return null;
  }
}

export default Loading;
