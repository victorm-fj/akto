import { StackNavigator } from 'react-navigation';
import Welcome from '../screens/welcome/Welcome';

export default StackNavigator(
  {
    Welcome: { screen: Welcome },
  },
  {
    headerMode: 'none',
  }
);
