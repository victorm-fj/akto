import I18n, { getLanguages } from 'react-native-i18n';
import en from './en.json';
import es from './es.json';

I18n.fallbacks = true;
I18n.translations = { en, es };
// I18n.locale = 'es-ES'; // hardcode locale string
getLanguages()
  .then((languages) => console.log('getLanguages languages', languages))
  .catch((error) => console.log('getLanguages error', error));

export const strings = (name, params = {}) => I18n.t(name, params);
export default I18n;
