import React from 'react';
import { Alert, Platform } from 'react-native';
import Permissions from 'react-native-permissions';

const permissionsMessages = {
  contacts: {
    title: 'Can we access your contacts list?',
    message: 'We need access so that we can send your invitations',
  },
};

export default (WrappedComponent, permissions) => {
  class RequirePermissions extends React.Component {
		state = { permissions: {} };
		componentDidMount() {
		  this.checkPermissions();
		}
		async checkPermissions() {
		  if (permissions.length > 1) {
		    try {
		      const response = await Permissions.checkMultiple(permissions);
		      this.setState({ permissions: response }, this.confirmPermissions());
		    } catch (error) {
		      console.log('error checking permissions status', error);
		    }
		  } else {
		    const permission = permissions[0];
		    const response = await Permissions.check(permission);
		    this.setState({ permissions: { [permission]: response } });
		    if (response === 'undetermined' || response === 'denied') {
		      this.askForPermission(permission);
		    }
		  }
		}
		confirmPermissions() {
		  permissions.forEach((permission) => {
		    const statePermission = this.state.permissions[permission];
		    if (
		      statePermission === 'undetermined' ||
					statePermission === 'denied'
		    ) {
		      this.askForPermission(permission);
		    }
		  });
		}
		askForPermission(permission) {
		  let platformOption = null;
		  const buttons = [];
		  const cancelButton = {
		    text: 'No way!',
		    onPress: () => console.log('Permission denied'),
		    style: 'cancel',
		  };
		  buttons.push(cancelButton);
		  const statePermission = this.state.permissions[permission];
		  if (Platform.OS === 'ios' && statePermission === 'denied') {
		    platformOption = {
		      text: 'Open Settings',
		      onPress: this.openSettings,
		    };
		  } else {
		    platformOption = {
		      text: 'OK',
		      onPress: () => this.requestPermission(permission),
		    };
		  }
		  buttons.push(platformOption);
		  Alert.alert(
		    permissionsMessages[permission].title,
		    permissionsMessages[permission].message,
		    buttons
		  );
		}
		// Only works for iOS
		async openSettings() {
		  try {
		    await Permissions.openSettings();
		    alert('Back to app!');
		  } catch (error) {
		    console.log('error opening settings', error);
		  }
		}
		async requestPermission(permission) {
		  const response = await Permissions.request(permission);
		  this.setState({
		    permissions: { ...this.state.permissions, [permission]: response },
		  });
		}
		render() {
		  return (
		    <WrappedComponent
		      {...this.props}
		      permissions={permissions}
		      permissionsStatus={this.state.permissions}
		    />
		  );
		}
  }

  return RequirePermissions;
};
