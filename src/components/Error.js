import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';

const Error = ({ message }) => (
  <View
    style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <Text style={{ padding: 20, fontSize: 20, fontWeight: 'bold' }}>
      {message}
    </Text>
  </View>
);

Error.propTypes = {
  message: PropTypes.string,
};
Error.defaultProps = {
  message: 'Error! Something went wrong.',
};
export default Error;
