import React from 'react';
import { View } from 'react-native';
import { Bubbles } from 'react-native-loader';
import PropTypes from 'prop-types';

const Loader = ({ color, size, backgroundColor }) => (
  <View
    style={{
      flex: 1,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor,
    }}
  >
    <Bubbles size={size} color={color} />
  </View>
);

Loader.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  backgroundColor: PropTypes.string,
};
Loader.defaultProps = {
  color: '#1F2341',
  size: 12,
  backgroundColor: 'rgba(0,0,0,0.25)',
};
export default Loader;
