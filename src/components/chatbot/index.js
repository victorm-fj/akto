import ChatBot from './ChatBot';
import Loading from './steps/common/Loading';

export { Loading };
export default ChatBot;
