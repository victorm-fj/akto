import { AsyncStorage } from 'react-native';
const JSON = require('circular-json');

export const getData = async (cacheName, firstStep, steps) => {
  const currentStep = firstStep;
  const renderedSteps = [steps[currentStep.id]];
  const previousSteps = [steps[currentStep.id]];
  const previousStep = {};

  try {
    const data = await AsyncStorage.getItem(cacheName);
    const dataObj = JSON.parse(data);
    const lastStep = dataObj.renderedSteps[dataObj.renderedSteps.length - 1];
    if (lastStep && lastStep.end) {
      removeData(cacheName);
    } else {
      dataObj.renderedSteps.forEach((renderedStep) => {
        renderedStep.delay = 0;
        renderedStep.rendered = true;
        if (renderedStep.component) {
          const id = renderedStep.id;
          renderedStep.component = steps[id].component;
        }
      });
      const { trigger, end, options } = dataObj.currentStep;
      const id = dataObj.currentStep.id;
      if (options) {
        delete dataObj.currentStep.rendered;
      }
      if (!trigger && !end) {
        if (options) {
          options.forEach(
            (option) =>
              (dataObj.currentStep[option].trigger = steps[id][option].trigger)
          );
        } else {
          dataObj.currentStep.trigger = steps[id].trigger;
        }
      }
      return dataObj;
    }
  } catch (error) {
    console.log('error in asyncstorage get item', error);
    return { currentStep, previousStep, previousSteps, renderedSteps };
  }
};

export const setData = (cacheName, data) => {
  return AsyncStorage.setItem(cacheName, JSON.stringify(data));
};

export const removeData = (cacheName) => {
  return AsyncStorage.removeItem(cacheName);
};
