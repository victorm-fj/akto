import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Bubble from './Bubble';
import Img from './Image';
import ImageContainer from './ImageContainer';
import Loading from '../common/Loading';
import TextStepContainer from './TextStepContainer';
import TextMessage from './TextMessage';

class TextStep extends Component {
	state = {
	  loading: true,
	};
	componentDidMount() {
	  const { step } = this.props;
	  const { component, delay, waitAction } = step;
	  const isComponentWatingUser = component && waitAction;
	  setTimeout(() => {
	    // this.props.triggerNextStep();
	    this.setState({ loading: false });
	    if (!isComponentWatingUser) {
	      this.props.triggerNextStep();
	    }
	  }, delay);
	}
	renderMessage = () => {
	  const { previousValue, step } = this.props;
	  const { component, fontColor } = step;
	  let { message } = step;
	  if (component) {
	    const { steps, previousStep, triggerNextStep } = this.props;
	    return React.cloneElement(component, {
	      step,
	      steps,
	      previousStep,
	      triggerNextStep,
	    });
	  }
	  message = message.replace(/{previousValue}/g, previousValue);
	  return <TextMessage fontColor={fontColor}>{message}</TextMessage>;
	};
	render() {
	  console.log('TextStep props -', this.props);
	  const {
	    step,
	    isFirst,
	    isLast,
	    avatarStyle,
	    bubbleStyle,
	    hideBotAvatar,
	    hideUserAvatar,
	  } = this.props;
	  const { avatar, bubbleColor, fontColor, user } = step;
	  const showAvatar = user ? !hideUserAvatar : !hideBotAvatar;
	  return (
	    <TextStepContainer className="rsc-ts" user={user}>
	      {isFirst &&
					showAvatar && (
	          <ImageContainer borderColor={bubbleColor} user={user}>
	            <Img
	              style={avatarStyle}
	              showAvatar={showAvatar}
	              user={user}
	              source={{ uri: avatar }}
	              alt="avatar"
	            />
	          </ImageContainer>
	        )}
	      <Bubble
	        style={bubbleStyle}
	        user={user}
	        bubbleColor={bubbleColor}
	        showAvatar={showAvatar}
	        isFirst={isFirst}
	        isLast={isLast}
	      >
	        {this.state.loading && <Loading color={fontColor} />}
	        {!this.state.loading && this.renderMessage()}
	      </Bubble>
	    </TextStepContainer>
	  );
	}
}

TextStep.propTypes = {
  isFirst: PropTypes.bool.isRequired,
  isLast: PropTypes.bool.isRequired,
  step: PropTypes.object.isRequired,
  triggerNextStep: PropTypes.func.isRequired,
  avatarStyle: PropTypes.object.isRequired,
  bubbleStyle: PropTypes.object.isRequired,
  hideBotAvatar: PropTypes.bool.isRequired,
  hideUserAvatar: PropTypes.bool.isRequired,
  previousStep: PropTypes.object,
  previousValue: PropTypes.any,
  steps: PropTypes.object,
};
TextStep.defaultProps = {
  previousStep: {},
  steps: {},
  previousValue: '',
};
export default TextStep;
