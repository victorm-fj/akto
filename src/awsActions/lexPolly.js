import AWS from 'aws-sdk';
import config from '../aws-exports';
// Initialize the Amazon Cognito credentials provider
AWS.config.region = config.aws_project_region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: config.aws_cognito_identity_pool_id,
});

// const lexRunTime = new AWS.LexRuntime();
// const botAlias = config.aws_bots_config[0].alias;
// const botName = config.aws_bots_config[0].name;
// const userId = 'AKTOAgent' + Date.now();
// export const postText = (inputText, sessionAttributes) => {
//   const params = {
//     botAlias,
//     botName,
//     inputText,
//     userId,
//     sessionAttributes,
//   };
//   return new Promise((resolve, reject) => {
//     lexRunTime.postText(params, (err, data) => {
//       if (err) {
//         console.log(err);
//         reject(err);
//         return;
//       }

//       resolve(data);
//     });
//   });
// };
// https://docs.aws.amazon.com/polly/latest/dg/voicelist.html
export const pollyVoices = {
  'da-DK': ['Mads, Naja'],
  'nl-NL': ['Lotte', 'Ruben'],
  'en-AU': ['Nicole', 'Russell'],
  'en-GB': ['Amy', 'Brian', 'Emma'],
  'en-IN': ['Aditi', 'Raveena'],
  'en-US': [
    'Ivy',
    'Joanna',
    'Joey',
    'Justin',
    'Kendra',
    'Kimberly',
    'Matthew',
    'Salli',
  ],
  'en-GB-WLS': ['Geraint'],
  'fr-FR': ['Celine', 'Mathieu'],
  'fr-CA': ['Chantal'],
  'de-DE': ['Hans', 'Marlene', 'Vicki'],
  'is-IS': ['Dora', 'Karl'],
  'it-IT': ['Carla', 'Giorgio'],
  'ja-JP': ['Mizuki', 'Takumi'],
  'ko-KR': ['Seoyeon'],
  'nb-NO': ['Liv'],
  'pl-PL': ['Jacek', 'Jan', 'Ewa', 'Maja'],
  'pt-BR': ['Ricardo', 'Vitoria'],
  'pt-PT': ['Cristiano', 'Ines'],
  'ro-RO': ['Carmen'],
  'ru-RU': ['Maxim', 'Tatyana'],
  'es-ES': ['Conchita', 'Enrique'],
  'es-US': ['Miguel', 'Penelope'],
  'sv-SE': ['Astrid'],
  'tr-TR': ['Filiz'],
  'cy-GB': ['Gwyneth'],
};

export const getLocale = (userLocale) => {
  let voicesId = pollyVoices[userLocale];
  if (!voicesId) {
    const languageSubstring = userLocale.slice(0, 2).toLowerCase();
    if (languageSubstring === 'es') {
      voicesId = pollyVoices['es-US'];
    } else {
      voicesId = pollyVoices['en-US'];
    }
  }
  return voicesId[0];
};

export const getAudioUrl = (message, locale) => {
  const presignerPolly = new AWS.Polly.Presigner();
  const voiceId = getLocale(locale);
  const params = {
    OutputFormat: 'mp3',
    Text: message,
    TextType: 'text',
    VoiceId: voiceId,
  };
  return new Promise((resolve, reject) => {
    presignerPolly.getSynthesizeSpeechUrl(
      params,
      [(expires = 300)],
      (err, url) => {
        if (err) {
          console.log(err);
          reject(err);
          return;
        }

        resolve(url);
      }
    );
  });
};
