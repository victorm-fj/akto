import { AppRegistry } from 'react-native';
import codePush from 'react-native-code-push';
import { Client } from 'bugsnag-react-native';
import App from './src/App';
const bugsnag = new Client();

AppRegistry.registerComponent('AKTO', () => codePush(App));

// The below line is necessary for use with the TestBed App
AppRegistry.registerComponent('ViroSample', () => codePush(App));
